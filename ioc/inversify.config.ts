import "reflect-metadata";
import {Container} from "inversify";
import {COMMON_TYPES} from "./commonTypes";
import {IFunctionService} from "../HttpTrigger/services/IFunctionService";
import {FunctionService} from "../HttpTrigger/services/FunctionService";
import {IPokeFetcher} from "../HttpTrigger/services/IPokeFetcher";
import {PokeFetcher} from "../HttpTrigger/services/PokeFetcher";
import {ILogger} from "../commonServices/ILogger";
import {Logger} from "../commonServices/Logger";
import {ICache} from "../commonServices/ICache";
import {Cache} from "../commonServices/Cache";

const getContainer: (() => Container) = (): Container => {
    const container: Container = new Container();

    container
        .bind<ILogger>(COMMON_TYPES.ILogger)
        .to(Logger)
        .inSingletonScope();

    container
        .bind<ICache>(COMMON_TYPES.ICache)
        .to(Cache)
        .inSingletonScope();

    container
        .bind<IPokeFetcher>(COMMON_TYPES.IPokeFetcher)
        .to(PokeFetcher);

    container
        .bind<IFunctionService<any>>(COMMON_TYPES.IFunctionService)
        .to(FunctionService);

    return container;
};

export default getContainer;
