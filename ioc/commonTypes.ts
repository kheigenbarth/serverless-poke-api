export const COMMON_TYPES: any = {
    ILogger: Symbol.for("ILogger"),
    IFunctionService: Symbol.for("IFunctionService"),
    IPokeFetcher: Symbol.for("IPokeFetcher"),
    ICache: Symbol.for("ICache"),
};
