export interface ICache {
    get(key: string | number): any;
    set(key: string | number, value: any): void;
}
