import {ICache} from "./ICache";
import {injectable} from "inversify";
import Redis from "ioredis";

@injectable()
export class Cache implements ICache {

    private _redis: Redis.Redis;
    private _cacheEnabled: boolean = false;

    public async setup(): Promise<void> {
        this._redis = new Redis({
            password: "!auth-pass7",
            maxRetriesPerRequest: 0,
            lazyConnect: true,
        });

        this._redis.on("error", () => {
            this._cacheEnabled = false;
        });

        await this._redis.connect().then((a) => {
            this._cacheEnabled = true;
        }).catch(() => {
            this._cacheEnabled = false;
        });
    }

    public async get(key: string | number): Promise<any> {
        if (this._cacheEnabled) {
            const cache: any = await this._redis.get(this.serializeToString(key));
            return this.deserialize(cache);
        }

        return null;
    }

    public async set(key: string | number, value: any): Promise<void> {
        if (this._cacheEnabled) {
            this._redis.set(this.serializeToString(key), this.serializeToString(value));
        }
    }

    private serializeToString(key: string | number): string {
        if (typeof key === "number" || typeof key === "object") {
            return JSON.stringify(key);
        }
        return key;
    }

    private deserialize(key: string): object {
        return JSON.parse(key);
    }
}
