export interface IPokeFetcher {
    fetchPokes(ids: string, type: string): Promise<any>;
}

export interface IPokeQuery {
    id: string;
    type: string;
}
