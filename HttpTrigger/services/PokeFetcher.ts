import axios from "axios";
import {IPokeFetcher} from "./IPokeFetcher";
import {inject, injectable} from "inversify";
import _ from "lodash";
import {COMMON_TYPES} from "../../ioc/commonTypes";
import {ICache} from "../../commonServices/ICache";
import {IPokemon} from "../interfaces/IPokemon";

@injectable()
export class PokeFetcher implements IPokeFetcher {

    @inject(COMMON_TYPES.ICache) private readonly _cache: ICache;
    private readonly _endpoint: string = "https://pokeapi.co/api/v2/pokemon/";

    public async fetchPokes(ids: string, type: string): Promise<any> {
        const parsedIds: number[] = ids.split(",").map(Number);
        let pokemons: IPokemon[] = _.map(parsedIds, (id: number) => this.fetchSinglePokeById(id));

        pokemons = await Promise.all(pokemons);
        pokemons = _.filter(pokemons, (p: IPokemon) => _.some(p.types, (types) => types.type.name === type));
        pokemons = _.map(pokemons, (p) => p.name);

        return {pokemons};
    }

    private async fetchSinglePokeById(id: number): Promise<any> {
        const cachedPokemon: IPokemon = await this._cache.get(id);

        if (cachedPokemon) {
            return cachedPokemon;
        }

        return axios.get(`${this._endpoint}${id}`)
            .then((r) => {
                this._cache.set(id, r.data);
                return r.data;
            })
            .catch((e) => console.log);
    }
}
