import {inject, injectable} from "inversify";
import {IFunctionService} from "./IFunctionService";
import {COMMON_TYPES} from "../../ioc/commonTypes";
import {IPokeFetcher, IPokeQuery} from "./IPokeFetcher";
import {ILogger} from "../../commonServices/ILogger";

@injectable()
export class FunctionService implements IFunctionService<any> {
    @inject(COMMON_TYPES.ILogger)
    private readonly _logger: ILogger;

    @inject(COMMON_TYPES.IPokeFetcher)
    private readonly _pokeFetcher: IPokeFetcher;

    public async processMessageAsync(msg: IPokeQuery): Promise<any> {
        this._logger.info("Hello world");
        this._logger.verbose(`${JSON.stringify(msg)}`);

        if (!msg.id || !msg.type) {
            return {
                message: "id and type are required",
            };
        }

        return this._pokeFetcher.fetchPokes(msg.id, msg.type);
    }
}
