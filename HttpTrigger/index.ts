import {AzureFunction, Context, HttpRequest} from "@azure/functions";
import getContainer from "../ioc/inversify.config";
import {COMMON_TYPES} from "../ioc/commonTypes";
import {Logger} from "../commonServices/Logger";
import {ILogger} from "../commonServices/ILogger";
import {IFunctionService} from "./services/IFunctionService";
import {Container} from "inversify";
import {Cache} from "../commonServices/Cache";
import {ICache} from "../commonServices/ICache";

const httpTrigger: AzureFunction = async (ctx: Context, req: HttpRequest): Promise<any> => {
    const container: Container = getContainer();

    const logger: Logger = container.get<ILogger>(COMMON_TYPES.ILogger) as Logger;
    const cache: Cache = container.get<ICache>(COMMON_TYPES.ICache) as Cache;

    logger.init(ctx, "1");
    await cache.setup();

    const functionService: IFunctionService<any> =
        container.get<IFunctionService<any>>(COMMON_TYPES.IFunctionService);

    const response: any = await functionService.processMessageAsync(req.query);

    ctx.res = {
        body: response,
        status: 200,
        headers: {"Content-Type": "application/json"},
    };
    return ctx.res;
};

export default httpTrigger;
