export interface IPokemon {
    types: IPokemonTypes[];
    name: string;
}

interface IPokemonTypes {
    slot: number;
    type: IPokemonType;
}

interface IPokemonType {
    name: string;
    url: string;
}
